# Blue Heron Bridge Buddy

(c) 2020 Cal Evans All Rights Reserved

Released under the MIT License

This program will give you high and low tide for Blue Heron Bridge. (NOAA Station #8722588) This is demonstration code to show how to use the symfony console component.

# Installation

1. Clone this repo locally
1. Run `composer install` in the repo
1. cd to the `app` dir
1. `./bhb <DATE>` where `<DATE>` is any date.
1. Read the code and see how it was done.