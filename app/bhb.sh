#!/usr/bin/env php
<?php

declare(strict_types=1);
$appPath = realpath(dirname(__FILE__) . '/../') . '/';

require_once $appPath . 'vendor/autoload.php';

use BHB\Commands\Tides;

use Symfony\Component\Console\Application;

$app = new Application('BHB Buddy', '1.0.0');

$tides = new Tides();

$app->addCommands(
  [
    $tides
  ]
);

$app->setDefaultCommand($tides->getName());

$app->run();
