<?php
declare(strict_types=1);

namespace BHB\Commands;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Helper\TableSeparator;

class Tides extends Command
{
  protected $station = '8722588';

  /**
   * Called by the application, this method sets up the command.
   */
    protected function configure()
    {
        $definition = [
          new InputOption('student', '', InputOption::VALUE_NONE, 'Calculate times for a student diver'),
          new InputOption('date', 'd', InputOption::VALUE_REQUIRED, 'The date to show'),
          new InputOption('station', 's', InputOption::VALUE_REQUIRED, 'The NOAA station to show','8722588'),
        ];

        $this->setName('tides')
           ->setDescription('List High tide for a given date')
           ->setDefinition($definition)
           ->setHelp(
               'This program will output the high tides for a given date.'
           );
        return;
    }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
    public function execute(
      InputInterface $input,
      OutputInterface $output
    ) : int {
      $output->writeln(' ', OutputInterface::VERBOSITY_NORMAL) ;

      /*
       * Fetch Inputs
       */
      $date = (string) $input->getOption('date');
      $station = (string) $input->getOption('station');
      $student = (bool) $input->getOption('student')??false;
      $date = new \DateTimeImmutable($date);
      $formattedDate = $date->format('Ymd');

      /*
       * DateTime Tricks
       */

      /*
       * Simple Date Addition
       */
      // $startDate = new \DateTimeImmutable();
      // $endDate = $startDate->add(new \DateInterval('P7D'));
      // echo "\nStart Date : " . $startDate->format('Y-m-d') . "\n";
      // echo "End Date   : " . $endDate->format('Y-m-d') . "\n";
      // die("\n");

      /*
       * Time Addition
       */
      // $startTime = new \DateTimeImmutable();
      // $endTime = $startTime->add(new \DateInterval('PT30M'));
      // echo "\nStart Time : " . $startTime->format('h:i') . "\n";
      // echo "End Time   : " . $endTime->format('h:i') . "\n";
      // die("\n");

      /*
       * strtotime
       */
      // print_r(
      //   (new \DateTime("first day of July"))
      //   ->format('m/d/Y')
      // );
      // die("\n");

      /*
       * Date formats
       */
      // print_r((new \DateTime("first day of July"))->format(\DateTimeInterface::ISO8601));
      // die("\n");

      /*
       * Date diffs
       */
      // $date1 = new \DateTime();
      // $date2 = new \DateTime("first day of July, 1999");
      // $diff = $date1->diff($date2);
      // print_r($diff);
      // die("\n");

      /*
       * timezones
       *

       * Timezones can be one of three different types in DateTime objects:

       *   Type 1: A UTC offset, such as in new DateTime("17 July 2013 -0300");
       *   Type 2: A timezone abbreviation, such as in new DateTime("17 July 2013 GMT");
       *   Type 3: A timezone identifier, such as in new DateTime( "17 July 2013", new DateTimeZone("Europe/London"));
       *
       */
      // print_r(new \DateTimeZone("UTC"));
      // $date = new \DateTime("2020-12-01 00:00:00", new \DateTimeZone("UTC"));
      // $date->setTimezone(new \DateTimeZone("America/Detroit"));
      // print_r($date->format("Y-m-d H:i:s"));
      // die("\n");

      /*
       * Iterating over a date range
       */
      // $startDate = new \DateTimeImmutable();
      // $endDate = $startDate->add(new \DateInterval('P7D'));
      // $interval = new \DateInterval("P1D");

      // $period = new \DatePeriod($startDate, $interval, 30);
      // //  $period = new \DatePeriod($startDate, $interval, $endDate);
      // print_r($period);
      // foreach ($period as $currentDate) {
      //     echo $currentDate->format("m/d/Y"), PHP_EOL;
      // }
      // die("\n");


      /*
       * Build the URL
       */
      $url = 'https://www.tidesandcurrents.noaa.gov/api/datagetter?' .
        'begin_date=' . $formattedDate .
        '&end_date=' . $formattedDate .
        '&station=' . $station .
        '&product=predictions' .
        '&datum=STND' .
        '&time_zone=lst_ldt' .
        '&interval=hilo' .
        '&units=english' .
        '&format=json';

        /*
         * Step 1: Fetch and decode the data
         */
         $tides = json_decode(file_get_contents($url))->predictions;

         /*
         * Step 2: Prepare to display the data
         */
        $tableStyle = new TableStyle();
        $tableStyle->setHeaderTitleFormat( '<fg=white;bg=black;options=bold> %s </>');

        $table = new Table($output);
        $table->setStyle($tableStyle);
        $table->setHeaderTitle( '<fg=green>' . $date->format('m/d/Y') . '</><fg=white> : </><fg=green>' . $station.'</>' );
        $table->setHeaders(['Event', 'Time']);
        $table->setColumnWidth(0, 15);
        $table->setColumnWidth(1, 10);

        /*
         * Step 3: Iterate over the data outputting the desired data
         */
        $rowCounter = 0;
        foreach ($tides as $tidalEvent) {
          if ($tidalEvent->type==='H') {
             /*
              * Calculate "Feet Wet"
              */
            $highTide = new \DateTimeImmutable($tidalEvent->t);
            $finsWetIntervalSpec = $student ? 'PT60M' : 'PT30M';
            $finsWet = $highTide->sub(new \DateInterval($finsWetIntervalSpec));

            /*
             * Calculate 10 minutes
             */
            $tenMinutes = $finsWet->sub(new \DateInterval('PT10M'));

            /*
             * Calculate 30 minutes
             */
            $thirtyMinutes = $finsWet->sub(new \DateInterval('PT30M'));

            /*
             * Calculate Fins Dry
             */
            $walkOutIntervalSpec = $student ? 'PT60M' : 'PT30M';
            $walkOut = $highTide->add(new \DateInterval($walkOutIntervalSpec));

            if ($rowCounter!==0) {
              $table->addRow(new TableSeparator());
            } else {
              $rowCounter++;
            }
            if ($student) {
              $table->addRow(['30 Minutes',$thirtyMinutes->format('h:i A')]);
            }
            $table->addRow(['10 Minutes',$tenMinutes->format('h:i A')]);
            $table->addRow(['Fins Wet',$finsWet->format('h:i A')]);
            $table->addRow(['<fg=yellow>High Tide</>','<fg=yellow>' . $highTide->format('h:i A') . '</>']);
            $table->addRow(['Walk Out',$walkOut->format('h:i A')]);

          }
        }

          /*
           * Step 4: Render the data
           */
        $table->render();
        $output->writeln('Done', OutputInterface::VERBOSITY_NORMAL) ;
        return 0;
    }

}
